const canvas = document.getElementById("the_canvas")
const context = canvas.getContext("2d");

let playerChar = new Image();
playerChar.src = "assets/img/Green-16x18-spritesheet.png";

const scale = 2;
const width = 16;
const height = 18;
const scaledWidth = scale * width;
const scaledHeight = scale * height;
const walkLoop = [0, 1, 0, 2];
const frameLimit = 7;

let currentLoopIndex = 0;
let frameCount = 0;
let currentDirection = 0;
let speed = 2;
let scoreCount = 0;
var val = 100;

function clickableDpadReleased() 
{
    console.log(event);
    gamerInput = new GamerInput("None");
}
function clickDpadYellow()
{
    console.log(event);
    gamerInput = new GamerInput("Up");
}
function clickDpadBlue()
{
    console.log(event);
    gamerInput = new GamerInput("Left");
}
function clickDpadRed()
{
    console.log(event);
    gamerInput = new GamerInput("Right");
}
function clickDpadGreen()
{
    console.log(event);
    gamerInput = new GamerInput("Down");
}

function GameObject(spritesheet, x, y, width, height) {
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.mvmtDirection = "None";
}

let player = new GameObject(playerChar, 500, 250, 200, 200);

function GamerInput(input) 
{
    this.action = input; 
}

let gamerInput = new GamerInput("None"); 

let yellowButton = document.getElementsByClassName("yellow")[0];
let blueButton = document.getElementsByClassName("blue")[0];
let redButton = document.getElementsByClassName("red")[0];
let greenButton = document.getElementsByClassName("green")[0];

function input(event) 
{
    if (event.type === "keydown") 
    {
        switch (event.keyCode) 
        {
            case 37: 
                gamerInput = new GamerInput("Left");
                blueButton.classList.add("pressed");
                break; 
            case 38: 
                gamerInput = new GamerInput("Up");
                yellowButton.classList.add("pressed");
                break; 
            case 39: 
                gamerInput = new GamerInput("Right");
                redButton.classList.add("pressed"); 
                break; 
            case 40: 
                gamerInput = new GamerInput("Down");
                greenButton.classList.add("pressed");
                break; 
            default:
                gamerInput = new GamerInput("None"); 
        }
    } 
    else 
    {
        gamerInput = new GamerInput("None");
        redButton.classList.remove("pressed");
        blueButton.classList.remove("pressed");
        yellowButton.classList.remove("pressed");
        greenButton.classList.remove("pressed");
    }
}


function update() {
    console.log("Update");
    
    if (gamerInput.action === "Up") 
    {
        console.log("Move Up");
        player.y -= speed;
        currentDirection = 1;
        yellowButton.classList.add("pressed");
    }
    else if (gamerInput.action === "Down") 
    {
        console.log("Move Down");
        player.y += speed;
        currentDirection = 0;
        greenButton.classList.add("pressed");
    } 
    else if (gamerInput.action === "Left") 
    {
        console.log("Move Left");
        player.x -= speed; 
        currentDirection = 2;
        blueButton.classList.add("pressed");
    } 
    else if (gamerInput.action === "Right") 
    {
        console.log("Move Right");
        player.x += speed; 
        currentDirection = 3;
        redButton.classList.add("pressed"); 
    }

    collision();
   // dPadMovement();
    
}



function scoreSetUp()
{
    let scoreString = "score: " + scoreCount;
    context.font = '20px sans-serif';
    context.fillStyle = "#0021FC";
    context.fillText(scoreString, 1000, 20);
}

function drawFrame(image, frameX, frameY, canvasX, canvasY) 
{
    context.drawImage(image,
                  frameX * width, frameY * height, width, height,
                  canvasX, canvasY, scaledWidth, scaledHeight);
}

function animate() 
{
    if (gamerInput.action != "None")
    {
        frameCount++;
        if (frameCount >= frameLimit) 
        {
            frameCount = 0;
            currentLoopIndex++;
            if (currentLoopIndex >= walkLoop.length) 
            {
                currentLoopIndex = 0;
            }
        }      
    }
    else
    {
        currentLoopIndex = 0;
    }
    drawFrame(player.spritesheet, walkLoop[currentLoopIndex], currentDirection, player.x, player.y);
}

function collision()
{
    if (player.x > 1060)
    {
        player.x = 1060;
        val--;
        scoreCount++;
    }
    if (player.x < 0)
    {
        player.x = 0;
        val--;
        scoreCount++;
    }
    if (player.y > 460)
    {
        player.y = 460;
        val--;
        scoreCount++;
    }
    if (player.y < 10)
    {
        player.y = 10;
        val--
        scoreCount++;
    }
}

function draw()
{
    context.clearRect(0, 0, canvas.width, canvas.height);
    drawHealthbar();
    animate();
    scoreSetUp();
}

function gameloop()
{
    update();
    draw();
    window.requestAnimationFrame(gameloop);
}

function drawHealthbar()
{
    var width = 300;
    var height = 20;
    var max = 100;
  
    context.fillStyle = "#000000";
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.fillRect(0, 0, width, height);
  
    context.fillStyle = "#00FF00";
    var fillVal = Math.min(Math.max(val / max, 0), 1);
    context.fillRect(0, 0, fillVal * width, height);
}



window.requestAnimationFrame(gameloop);
window.addEventListener('keydown', input);
window.addEventListener('keyup', input); 
